class OrderReport < ActiveRecord::Base
  require 'csv'

  def self.import(file)
    CSV.foreach(file.path, {:headers => true, :col_sep => "\t", :quote_char => "Ð", :encoding => 'ISO-8859-1'}) do |row|

      or_hash = {
        order_id: row[0],
        order_item_id: row[1],
        purchase_date: row[2],
        payments_date: row[3],
        buyer_email: row[4],
        buyer_name: row[5],
        buyer_phone_number: row[6],
        sku: row[7],
        product_name: row[8],
        quantity_purchased: row[9],
        currency: row[10],
        item_price: row[11],
        item_tax: row[12],
        shipping_price: row[13],
        shipping_tax: row[14],
        ship_service_level: row[15],
        recipient_name: row[16],
        ship_address_1: row[17],
        ship_address_2: row[18],
        ship_address_3: row[19],
        ship_city: row[20],
        ship_state: row[21],
        ship_postal_code: row[22],
        ship_country: row[23],
        ship_phone_number: row[24],
        item_promotion_discount: row[25],
        item_promotion_id: row[26],
        ship_promotion_discount: row[27],
        ship_promotion_id: row[28],
        delivery_start_date: row[29],
        delivery_end_date: row[30],
        delivery_time_zone: row[31],
        delivery_Instructions: row[32],
        sales_channel: row[33],
        order_channel: row[34],
        order_channel_instance: row[35],
        external_order_id: row[36],
        is_business_order: row[37],
        purchase_order_number: row[38],
        price_designation: row[39]
      }
      orep = OrderReport.where(id: or_hash["id"])

      if orep.count == 1
        orep.first.update_attributes(or_hash)
      else
        OrderReport.create!(or_hash)
      end 
    end 
  end
end
