class OrderConfirm < ActiveRecord::Base
   def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|

     oc_hash = {
         total: row[0]
     }
      oc = OrderConfirm.where(id: oc_hash["id"])

      if oc.count == 1
        oc.first.update_attributes(oc_hash)
      else
        oc.create!(oc_hash)
      end 
    end 
   end 
end
