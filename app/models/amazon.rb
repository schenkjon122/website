class Amazon < ActiveRecord::Base
  def self.to_csv
    CSV.generate(:col_sep => "\t") do |csv|
      columns = %w(order_id order_item_id quantity ship_date carrier_code carrier_name tracking_number ship_method)
      csv << ["order-id", "order-item-id", "quantity", "ship-date", "carrier-code", "carrier-name", "tracking-number", "ship-method"]
      all.each do |item|
        csv << item.attributes.values_at(*columns)
      end
    end
  end
end

