class Rsc < ActiveRecord::Base
  require 'csv'

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|

      rsc_hash = row.to_hash
      rsc = Rsc.where(id: rsc_hash["id"])

      if rsc.count == 1
        rsc.first.update_attributes(rsc_hash)
      else
        Rsc.create!(rsc_hash)
      end 
    end 
  end 
  def self.to_csv
    CSV.generate do |csv|
      columns = %w(mbr_po tracking_no data)
      csv << columns
      all.each do |item|
        csv << item.attributes.values_at(*columns)
      end
    end
  end
end