class InventoryController < ApplicationController
    def inventory
        
        def NJ
            #get each sku that nj supply has enabled
            @step=Sku_Table.where("NJ",True)
            #step through them
            @step.each do |t|
                #initialize db for new entry
                @NJ=NJ.new
                #assign values
                @NJ.pid=t.seller_sku
                #determind sky type
                if (t.sku.chars.count=6)
                    @NJ.stock=DIB.find_by("sku",t.sku).qty
                else
                    @NJ.stock=Orgill.find_by("sku",t.sku).qty
                end
                #qty in ny
                @NJ.extra_field_9 = oh07out.find_by("sku",t.sku).qty
                @NJ.save
            end    
        end
        #repeat steps above for other things
        def Sears
             @step=Sku_Table.where("Sears",True)
             @step.each do |t|
                 @sears = Sears.new
                 @sears.Item_id=@step.seller_sku
                 @sears.Location_id="627761061"
                if (t.sku.chars.count=6)
                    @sears.updated_available=DIB.find_by("sku",t.sku).qty
                else
                    @sears.updated_available=Orgill.find_by("sku",t.sku).qty
                end
                 @sears.Low_Inventory_Threshold='0'
                 @sears.Pickup_Now_Eligible="N"
                 @sears.save
             end
        end
    end
end
