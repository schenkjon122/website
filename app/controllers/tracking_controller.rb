class TrackingController < ApplicationController
  
  def index
    @rsc = Rsc.all
  end
  
  def show
    @rsc = Rsc.find(params[:id])
  end
  
  def new
    @rsc = Rsc.new
  end
  
  def create
    @rsc = Rsc.new(rsc_params)
    if @rsc.save
      if @rsc.tracking_no = "Not Shipped"
        @rsc.cat = "Misc"
        @rsc.data = Date.yesterday.strftime("%m/%d/%Y")        
      elsif @rsc.mbr_po=~/^NJ-/
        @rsc.cat="NJ"
        @rsc.data = Date.yesterday.strftime("%m/%d/%Y")
      elsif  @rsc.mbr_po=~/^[[:digit:]]{3}-[[:digit:]]/
        @rsc.cat = "Amazon"
        @rsc.data = Date.yesterday.strftime("%m/%d/%Y")           
      elsif  @rsc.mbr_po=~/^MP/
        @rsc.cat = "Global"
        @rsc.data = Date.yesterday.strftime("%m/%d/%Y")           
      elsif  @rsc.mbr_po=~/^9........$/
        @rsc.cat = "Sears"
        @rsc.data = Date.yesterday.strftime("%m/%d/%Y")   
      elsif @rsc.mbr_po=~/^[456]....$/
        @rsc.cat = "TTW"
        @rsc.data = "Shipped" 
      else
        @rsc.cat = "Misc"
        @rsc.data = Date.yesterday.strftime("%m/%d/%Y")   
      end
      @rsc.save
      flash[:success] = "RSC Added"
      redirect_to '/tracking/new'
    end
  end
  
  
  
  def NJ
    @data=Rsc.where(cat: "NJ")
    respond_to do |format|
      format.html
      format.csv {send_data @data.to_csv}
    end
  end
  
  def Amazon
    @data=Rsc.where(cat:"Amazon")
    @amazon=Amazon.all
    respond_to do |format|
      format.html
      format.csv {send_data @amazon.to_csv}
    end
  end
  
  def TTW
    @data=Rsc.where(cat:"TTW")
    respond_to do |format|
      format.html
      format.csv {send_data @data.to_csv}
    end
  end
  
  def Global
    @data=Rsc.where(cat:"Global")
    respond_to do |format|
      format.html
      format.csv {send_data @data.to_csv}
    end
  end
  
  def Sears
    @data=Rsc.where(cat:"Sears")  
    respond_to do |format|
      format.html
      format.csv {send_data @data.to_csv}
    end
  end
  
  def Misc
    @data=Rsc.where(cat:"Misc")  
    respond_to do |format|
      format.html
      format.csv {send_data @data.to_csv}
    end
  end
  

  def edit
    @rsc = Rsc.find(params[:id])
  end
  
  def update
    @rsc = Rsc.find(params[:id])
      if @rsc.tracking_no = "Not Shipped"
        @rsc.cat = "Misc"
        @rsc.data = Date.yesterday.strftime("%m/%d/%Y")        
      elsif @rsc.mbr_po=~/^NJ-/
        @rsc.cat="NJ"
        @rsc.data = Date.yesterday.strftime("%m/%d/%Y")
      elsif  @rsc.mbr_po=~/^[[:digit:]]{3}-[[:digit:]]/
        @rsc.cat = "Amazon"
        @rsc.data = Date.yesterday.strftime("%m/%d/%Y")           
      elsif  @rsc.mbr_po=~/^MP/
        @rsc.cat = "Global"
        @rsc.data = Date.yesterday.strftime("%m/%d/%Y")           
      elsif  @rsc.mbr_po=~/^9........$/
        @rsc.cat = "Sears"
        @rsc.data = Date.yesterday.strftime("%m/%d/%Y")   
      elsif @rsc.mbr_po=~/^[456]....$/
        @rsc.cat = "TTW"
        @rsc.data = "Shipped" 
      else
        @rsc.cat = "Misc"
        @rsc.data = Date.yesterday.strftime("%m/%d/%Y")   
      end
      @rsc.save
      flash[:success] = "RSC Updated"
      redirect_to '/tracking/new'
  end
  
  def destroy
    Rsc.find(params[:id]).destroy
    flash[:success] = "Rsc Deleted"
    redirect_to '/'
  end
  
  def orderreport
    OrderReport.delete_all
    OrderReport.import(params[:file]) 
    flash[:notice] = "Order Report Imported"
    redirect_to '/tracking/new'
  end
  
  def ordconfirm
    @ordcnfm = OrderConfirm.all
    @report = OrderReport
    @ordcnfm.each do |t|
      @object=t.total
      @length=@object.length
      @error = ErrorReport.new
      if (@object[95,1]!=" " && @object[96,1]!=" " && @object[97,10]!="          "  && @object[97,10]!=nil )
        @error.Sku = @object[25,6]
        @error.Qty = @object[31,5].to_i
        @short = @object[97,10]
        if (@report.where("substr(order_id,1,10) in (?)", @short)!=[])
          @stepper=@report.where("substr(order_id,1,10) in (?)", @short)
          @stepper.each do |test|
            @error.Order = test.order_id
          end
        else
          @error.Order=@short
        end
        @error.Reported_Date = Date.today.strftime("%m/%d/%Y")   
        @error.Status = "Error"
        @error.save
      end
    end
  flash[:notice] =  "Error Report Generated"
  redirect_to root_url
  end
  
  def order_confirm
    OrderConfirm.delete_all
    OrderConfirm.import(params[:file]) 
    flash[:notice] =  "Order Confirmation Imported"
    redirect_to '/tracking/new' 
  end
  
  def errorreport
   @errorreport = ErrorReport.all
  end
  
  def import
    Rsc.delete_all
    Amazon.delete_all
    Rsc.import(params[:file])
    @rsc = Rsc.all
    @rsc.each do |t|
      if t.tracking_no == "Not Shipped"
        t.cat = "Misc"
        t.data = previous_day(Date.today).strftime("%m/%d/%Y")      
      elsif t.mbr_po=~/^NJ-/
        t.cat="NJ"
        t.data = previous_day(Date.today).strftime("%m/%d/%Y")   
      elsif t.mbr_po=~/^[[:digit:]]{3}-[[:digit:]]/
       t.cat = "Amazon"
       @report = OrderReport
       @amazon = Amazon.new
       @column = @report.where("substr(order_id,1,10) in (?)", t.mbr_po)  
       @column.each do |a|
       @amazon.order_id = a.order_id
       @amazon.order_item_id = a.order_item_id
       @amazon.quantity = a.quantity_purchased
       if (amazondate(a.purchase_date)< Date.today)
         @amazon.ship_date = amazondate(a.purchase_date)
       else
         @amazon.ship_date = previous_day(Date.today).strftime("%m/%d/%Y")  
       end
       @amazon.carrier_name=''
       @amazon.tracking_number=t.tracking_no
       if t.tracking_no=~/^9/
         @amazon.carrier_code = "USPS"
       else
         @amazon.carrier_code = "UPS"
       end
       @amazon.ship_method = a.ship_service_level
       @amazon.save
     end
      elsif  t.mbr_po=~/^MP/
        t.cat = "Global"
        t.data = previous_day(Date.today).strftime("%m/%d/%Y")   
      elsif  t.mbr_po=~/^9........$/
       t.cat = "Sears"
        t.data = previous_day(Date.today).strftime("%m/%d/%Y")           
      elsif t.mbr_po=~/^[456]....$/
        t.cat = "TTW"
        t.data = "Shipped"
      else
       t.cat = "Misc"
        t.data = previous_day(Date.today).strftime("%m/%d/%Y")   
      end
      t.save
    end
    flash[:notice] = "RSC Imported"
    redirect_to root_url
  end
    
  def outputer
  redirect '/output/nj'
  end
 
 
 
 
  private
  
    def rsc_params
      params.require(:rsc).permit!
    end
    
    def previous_day(date)
      date += -1
      while (date.wday % 7 == 0) or (date.wday%7 == 6) do
        date += -1
      end
      date
    end
    
    def amazondate(date)
      date = Date.parse(date)+2.day
      while (date.wday % 7 == 0) or (date.wday%7 == 6) do
        date += 1
      end
      date
    end
end
