<!-- edit via https://jbt.github.io/markdown-editor/ -->

What is this repository for? 
------
WIP.This is a web based ERP to control, mantain, and perform the daily responsilities of a company dependent on certain factors.
This ERP is configured for a certain company in order to replace the plethora of spreadsheets that currently exist.

 How do I get set up?
------

The server running this website needs to have a fairly recent version of the following:

* Ruby
* Rails
* Puma
* mySQL

 It is currently working on ruby version: 2.3.0p0. Rails version: 4.2.2. and Puma version 3.1.0.
The other files and versions are found in the gemfile. 


 Who do I talk to? 
------

Jonathan Schenk

(e) jonschenk2@gmail.com

(p) 732-320-1657