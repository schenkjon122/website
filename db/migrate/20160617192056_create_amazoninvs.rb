class CreateAmazoninvs < ActiveRecord::Migration
  def change
    create_table :amazoninvs do |t|
      t.text :sku
      t.text :product_id
      t.text :product_id_type
      t.text :price
      t.text :item_condition
      t.integer :quantity
      t.text :add_delete
      t.text :expedited_shipping
      t.text :standard_plus
      t.text :item_note
      t.text :fulfillment_center_id
      t.text :product_tax_code
      t.text :leadtime_to_ship

      t.timestamps null: false
    end
  end
end
