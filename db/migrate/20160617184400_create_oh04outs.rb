class CreateOh04outs < ActiveRecord::Migration
  def change
    create_table :oh04outs do |t|
      t.string :total

      t.timestamps null: false
    end
  end
end
