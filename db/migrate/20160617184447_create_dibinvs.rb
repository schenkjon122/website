class CreateDibinvs < ActiveRecord::Migration
  def change
    create_table :dibinvs do |t|
      t.text :sku
      t.integer :qty

      t.timestamps null: false
    end
  end
end
