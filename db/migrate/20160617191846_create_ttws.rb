class CreateTtws < ActiveRecord::Migration
  def change
    create_table :ttws do |t|
      t.text :ProductID
      t.text :Name
      t.text :PartNumber
      t.integer :Quantity_In_Stock

      t.timestamps null: false
    end
  end
end
