class CreateSearsinvs < ActiveRecord::Migration
  def change
    create_table :searsinvs do |t|
      t.text :Item_Id
      t.text :Variation_Group_Id
      t.text :Location_Id
      t.text :Last_Provided_by_Seller_Quantity
      t.text :Existing_Reserved_Quantity
      t.text :Existing_Available_Quantity
      t.text :Existing_Inventory_Update_Timestamp
      t.text :Updated_Available_Quantity
      t.text :Updated_Inventory_Update_Timestamp
      t.text :Low_Inventory_Threshold
      t.text :Pickup_Now_Eligible

      t.timestamps null: false
    end
  end
end
