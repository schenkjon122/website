class CreateNjs < ActiveRecord::Migration
  def change
    create_table :njs do |t|
      t.text :pid
      t.integer :stock
      t.integer :extra_field_9

      t.timestamps null: false
    end
  end
end
