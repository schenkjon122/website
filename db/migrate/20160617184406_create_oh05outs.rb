class CreateOh05outs < ActiveRecord::Migration
  def change
    create_table :oh05outs do |t|
      t.string :total

      t.timestamps null: false
    end
  end
end
