class CreateOh03outs < ActiveRecord::Migration
  def change
    create_table :oh03outs do |t|
      t.string :total

      t.timestamps null: false
    end
  end
end
