class CreateSkuTables < ActiveRecord::Migration
  def change
    create_table :sku_tables do |t|
      t.text :Vendor_sku
      t.text :Vendor
      t.integer :Min_vendor
      t.integer :Site_min
      t.integer :Total
      t.boolean :Amazon, :default => false
      t.boolean :CA, :default => false
      t.boolean :Sears, :default => false
      t.boolean :Sears2, :default => false
      t.boolean :TTW, :default => false
      t.boolean :NJ, :default => false
      t.boolean :KHM, :default => false
      t.boolean :Global_Industrial, :default => false

      t.timestamps null: false
    end
  end
end
