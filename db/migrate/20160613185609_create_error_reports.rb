class CreateErrorReports < ActiveRecord::Migration
  def change
    create_table :error_reports do |t|
      t.string :Reported_Date
      t.string :Order
      t.string :Sku
      t.string :Qty
      t.string :Status
      t.string :Note

      t.timestamps null: false
    end
  end
end
