class CreateTtwProductids < ActiveRecord::Migration
  def change
    create_table :ttw_productids do |t|
      t.text :Min_vendor
      t.text :Site_min
      t.text :Part_number
      t.timestamps null: false
    end
  end
end
