class CreateAmazons < ActiveRecord::Migration
  def change
    create_table :amazons do |t|
      t.string :order_id
      t.string :order_item_id
      t.integer :quantity
      t.string :ship_date
      t.string :carrier_code
      t.string :carrier_name
      t.string :tracking_number
      t.string :ship_method

      t.timestamps null: false
    end
  end
end
