class CreateOh02outs < ActiveRecord::Migration
  def change
    create_table :oh02outs do |t|
      t.string :total

      t.timestamps null: false
    end
  end
end
