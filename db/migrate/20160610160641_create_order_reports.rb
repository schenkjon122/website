class CreateOrderReports < ActiveRecord::Migration
  def change
    create_table :order_reports do |t|
      t.string :order_id
      t.string :order_item_id
      t.string :purchase_date
      t.string :payments_date
      t.string :buyer_email
      t.string :buyer_name
      t.string :buyer_phone_number
      t.string :sku
      t.string :product_name
      t.string :quantity_purchased
      t.string :currency
      t.string :item_price
      t.string :item_tax
      t.string :shipping_price
      t.string :shipping_tax
      t.string :ship_service_level
      t.string :recipient_name
      t.string :ship_address_1
      t.string :ship_address_2
      t.string :ship_address_3
      t.string :ship_city
      t.string :ship_state
      t.string :ship_postal_code
      t.string :ship_country
      t.string :ship_phone_number
      t.string :item_promotion_discount
      t.string :item_promotion_id
      t.string :ship_promotion_discount
      t.string :ship_promotion_id
      t.string :delivery_start_date
      t.string :delivery_end_date
      t.string :delivery_time_zone
      t.string :delivery_Instructions
      t.string :sales_channel
      t.string :order_channel
      t.string :order_channel_instance
      t.string :external_order_id
      t.string :is_business_order
      t.string :purchase_order_number
      t.string :price_designation

      t.timestamps null: false
    end
  end
end
