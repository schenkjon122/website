class CreateOrderConfirms < ActiveRecord::Migration
  def change
    create_table :order_confirms do |t|
      t.text :total

      t.timestamps null: false
    end
  end
end
