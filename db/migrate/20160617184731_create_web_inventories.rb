class CreateWebInventories < ActiveRecord::Migration
  def change
    create_table :web_inventories do |t|
      t.text :sku
      t.text :wh
      t.integer :qty

      t.timestamps null: false
    end
  end
end
