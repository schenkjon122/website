class CreateUnshippedReports < ActiveRecord::Migration
  def change
    create_table :unshipped_reports do |t|
      t.text :order_id
      t.text :order_item_id
      t.text :purchase_date
      t.text :payments_date
      t.text :reporting_date
      t.text :promise_date
      t.text :days_past_promise
      t.text :buyer_email
      t.text :buyer_name
      t.text :buyer_phone_number
      t.text :sku
      t.text :product_name
      t.integer :quantity_purchased
      t.integer :quantity_shipped
      t.integer :quantity_to_ship
      t.text :ship_service_level
      t.text :recipient_name
      t.text :ship_address_1
      t.text :ship_address_2
      t.text :ship_address_3
      t.text :ship_city
      t.text :ship_state
      t.text :ship_postal_code
      t.text :ship_country
      t.text :is_business_order
      t.text :purchase_order_number
      t.text :price_designation

      t.timestamps null: false
    end
  end
end
