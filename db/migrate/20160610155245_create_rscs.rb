class CreateRscs < ActiveRecord::Migration
  def change
    create_table :rscs do |t|
      t.string :order_no
      t.string :invoice
      t.string :mbr_po
      t.string :ship_to_name
      t.string :ship_to_address
      t.string :ship_to_attention
      t.string :ship_to_city
      t.string :ship_to_state
      t.string :ship_to_zip
      t.string :tracking_no
      t.string :freight
      t.string :RSC
      t.string :cust_po
      t.string :sku
      t.string :shipper_id
      t.string :weight
      t.timestamps null: false
    end
  end
end
