class CreateOh06outs < ActiveRecord::Migration
  def change
    create_table :oh06outs do |t|
      t.string :total

      t.timestamps null: false
    end
  end
end
