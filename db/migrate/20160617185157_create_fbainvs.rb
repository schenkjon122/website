class CreateFbainvs < ActiveRecord::Migration
  def change
    create_table :fbainvs do |t|
      t.text :seller_sku
      t.text :fulfillment_channel_sku
      t.text :asin
      t.text :condition_type
      t.text :Warehouse_Condition_code
      t.integer :Quantity
      t.text :Available

      t.timestamps null: false
    end
  end
end
