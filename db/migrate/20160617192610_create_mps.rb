class CreateMps < ActiveRecord::Migration
  def change
    create_table :mps do |t|
      t.text :Seller_Sku
      t.text :Your_Price
      t.text :MSRP
      t.integer :Inventory
      t.timestamps null: false
    end
  end
end
