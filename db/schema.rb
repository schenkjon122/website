# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160617192610) do

  create_table "amazoninvs", force: :cascade do |t|
    t.text     "sku"
    t.text     "product_id"
    t.text     "product_id_type"
    t.text     "price"
    t.text     "item_condition"
    t.integer  "quantity"
    t.text     "add_delete"
    t.text     "expedited_shipping"
    t.text     "standard_plus"
    t.text     "item_note"
    t.text     "fulfillment_center_id"
    t.text     "product_tax_code"
    t.text     "leadtime_to_ship"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "amazons", force: :cascade do |t|
    t.string   "order_id"
    t.string   "order_item_id"
    t.integer  "quantity"
    t.string   "ship_date"
    t.string   "carrier_code"
    t.string   "carrier_name"
    t.string   "tracking_number"
    t.string   "ship_method"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",         default: 0, null: false
    t.integer  "attempts",         default: 0, null: false
    t.text     "handler",                      null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "progress_stage"
    t.integer  "progress_current", default: 0
    t.integer  "progress_max",     default: 0
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority"

  create_table "dibinvs", force: :cascade do |t|
    t.text     "sku"
    t.integer  "qty"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "error_reports", force: :cascade do |t|
    t.string   "Reported_Date"
    t.string   "Order"
    t.string   "Sku"
    t.string   "Qty"
    t.string   "Status"
    t.string   "Note"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "fbainvs", force: :cascade do |t|
    t.text     "seller_sku"
    t.text     "fulfillment_channel_sku"
    t.text     "asin"
    t.text     "condition_type"
    t.text     "Warehouse_Condition_code"
    t.integer  "Quantity"
    t.text     "Available"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "giinfos", force: :cascade do |t|
    t.text     "Seller_sku"
    t.text     "Your_price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mps", force: :cascade do |t|
    t.text     "Seller_Sku"
    t.text     "Your_Price"
    t.text     "MSRP"
    t.integer  "Inventory"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "njs", force: :cascade do |t|
    t.text     "pid"
    t.integer  "stock"
    t.integer  "extra_field_9"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "oh01outs", force: :cascade do |t|
    t.string   "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oh02outs", force: :cascade do |t|
    t.string   "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oh03outs", force: :cascade do |t|
    t.string   "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oh04outs", force: :cascade do |t|
    t.string   "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oh05outs", force: :cascade do |t|
    t.string   "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oh06outs", force: :cascade do |t|
    t.string   "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oh07outs", force: :cascade do |t|
    t.string   "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oh08outs", force: :cascade do |t|
    t.string   "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ordcnfm_010s", force: :cascade do |t|
    t.string   "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "order_confirms", force: :cascade do |t|
    t.text     "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "order_reports", force: :cascade do |t|
    t.string   "order_id"
    t.string   "order_item_id"
    t.string   "purchase_date"
    t.string   "payments_date"
    t.string   "buyer_email"
    t.string   "buyer_name"
    t.string   "buyer_phone_number"
    t.string   "sku"
    t.string   "product_name"
    t.string   "quantity_purchased"
    t.string   "currency"
    t.string   "item_price"
    t.string   "item_tax"
    t.string   "shipping_price"
    t.string   "shipping_tax"
    t.string   "ship_service_level"
    t.string   "recipient_name"
    t.string   "ship_address_1"
    t.string   "ship_address_2"
    t.string   "ship_address_3"
    t.string   "ship_city"
    t.string   "ship_state"
    t.string   "ship_postal_code"
    t.string   "ship_country"
    t.string   "ship_phone_number"
    t.string   "item_promotion_discount"
    t.string   "item_promotion_id"
    t.string   "ship_promotion_discount"
    t.string   "ship_promotion_id"
    t.string   "delivery_start_date"
    t.string   "delivery_end_date"
    t.string   "delivery_time_zone"
    t.string   "delivery_Instructions"
    t.string   "sales_channel"
    t.string   "order_channel"
    t.string   "order_channel_instance"
    t.string   "external_order_id"
    t.string   "is_business_order"
    t.string   "purchase_order_number"
    t.string   "price_designation"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "rscs", force: :cascade do |t|
    t.string   "order_no"
    t.string   "invoice"
    t.string   "mbr_po"
    t.string   "ship_to_name"
    t.string   "ship_to_address"
    t.string   "ship_to_attention"
    t.string   "ship_to_city"
    t.string   "ship_to_state"
    t.string   "ship_to_zip"
    t.string   "tracking_no"
    t.string   "freight"
    t.string   "RSC"
    t.string   "cust_po"
    t.string   "sku"
    t.string   "shipper_id"
    t.string   "weight"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "cat"
    t.string   "data"
  end

  create_table "searsinvs", force: :cascade do |t|
    t.text     "Item_Id"
    t.text     "Variation_Group_Id"
    t.text     "Location_Id"
    t.text     "Last_Provided_by_Seller_Quantity"
    t.text     "Existing_Reserved_Quantity"
    t.text     "Existing_Available_Quantity"
    t.text     "Existing_Inventory_Update_Timestamp"
    t.text     "Updated_Available_Quantity"
    t.text     "Updated_Inventory_Update_Timestamp"
    t.text     "Low_Inventory_Threshold"
    t.text     "Pickup_Now_Eligible"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "sku_tables", force: :cascade do |t|
    t.text     "Vendor_sku"
    t.text     "Vendor"
    t.integer  "Min_vendor"
    t.integer  "Site_min"
    t.integer  "Total"
    t.boolean  "Amazon",            default: false
    t.boolean  "CA",                default: false
    t.boolean  "Sears",             default: false
    t.boolean  "Sears2",            default: false
    t.boolean  "TTW",               default: false
    t.boolean  "NJ",                default: false
    t.boolean  "KHM",               default: false
    t.boolean  "Global_Industrial", default: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "ttw_productids", force: :cascade do |t|
    t.text     "Min_vendor"
    t.text     "Site_min"
    t.text     "Part_number"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "ttws", force: :cascade do |t|
    t.text     "ProductID"
    t.text     "Name"
    t.text     "PartNumber"
    t.integer  "Quantity_In_Stock"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "unshipped_reports", force: :cascade do |t|
    t.text     "order_id"
    t.text     "order_item_id"
    t.text     "purchase_date"
    t.text     "payments_date"
    t.text     "reporting_date"
    t.text     "promise_date"
    t.text     "days_past_promise"
    t.text     "buyer_email"
    t.text     "buyer_name"
    t.text     "buyer_phone_number"
    t.text     "sku"
    t.text     "product_name"
    t.integer  "quantity_purchased"
    t.integer  "quantity_shipped"
    t.integer  "quantity_to_ship"
    t.text     "ship_service_level"
    t.text     "recipient_name"
    t.text     "ship_address_1"
    t.text     "ship_address_2"
    t.text     "ship_address_3"
    t.text     "ship_city"
    t.text     "ship_state"
    t.text     "ship_postal_code"
    t.text     "ship_country"
    t.text     "is_business_order"
    t.text     "purchase_order_number"
    t.text     "price_designation"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "web_inventories", force: :cascade do |t|
    t.text     "sku"
    t.text     "wh"
    t.integer  "qty"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
