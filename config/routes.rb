Rails.application.routes.draw do
  root 'static#home'
  
  get 'tracking/nj' => 'tracking#NJ'
  get 'tracking/amazon' => 'tracking#Amazon'
  get 'tracking/ttw' => 'tracking#TTW'
  get 'tracking/global' => 'tracking#Global'
  get 'tracking/sears' => 'tracking#Sears'
  get 'tracking/misc' => 'tracking#Misc'
  get 'tracking/ordconfirm' => 'tracking#ordconfirm'
  get 'tracking/output' => 'tracking#outputer'
  get 'tracking/errorreport' => 'tracking#errorreport'
  
  resources:tracking do
    collection { post :import }
    collection { post :orderreport}
    collection { post :order_confirm}
  end

end
